<?php
$input = array_map('intval', explode(',', file_get_contents('input')));
$crabs = $input;

$mid = $crabs[floor(count($crabs)/2)];
$solution1  = array_sum(array_map(static fn(int $crab) => abs($crab - $mid), $crabs));

$solution2 = INF;
for ($i = 0, $iMax = count($crabs); $i < $iMax; $i++) {
   $solution2 = min(
       $solution2,
       array_sum(
           array_map(
           static function(int $crab) use ($i){
               $move = abs($crab - $i); 
               return floor($move * ($move + 1) / 2);
           } ,
           $crabs
           )
       )
   ) ;
}

echo "Solution Day 07-1: $solution1\n";
echo "Solution Day 07-2: $solution2\n";