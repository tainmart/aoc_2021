<?php

$input = explode("\n\n", trim(file_get_contents('input')));
$numbers = array_shift($input);

$cards = array_map(
    static fn(string $card) => array_map(
        static fn(string $line) => preg_split('/\s+/', trim($line)),
        explode("\n", $card)
    ),
    $input
);

$solution1 = null;
$solution2 = null;
foreach(explode(',', $numbers) as $index => $number) {
    // set matching numbers null on card
    array_walk_recursive(
        $cards,
        static function(&$cardNumber) use($number) {
            if ($cardNumber === $number) {
                $cardNumber = null;
            }
        }
    );
    if ($index < 4) {
        continue; // need at least 5 marks to mark/find a row
    }
    // check for bingo on card
    foreach ($cards as $cardNumber => $card) {
        $flippedCard = array_map(null, ...$card); // flipping row/columns
        if (count(array_filter($card, static fn(array $line) => checkBingo($line))) > 0 // columns
            || count(array_filter($flippedCard, static fn(array $line) => checkBingo($line))) > 0 // rows
        ) { 
            // BINGO!
            $sumRemaining = array_reduce(
                $cards[$cardNumber],
                static function(int $sum, array $line) {
                    $sum += array_sum(array_map('intval', $line));
                    return $sum;
                },
                0
            );
            if ($solution1 === null) {
                $solution1 = $number * $sumRemaining;
            }

            if (count($cards) > 1) {
                unset($cards[$cardNumber]);
                continue;
            }
            $solution2 = $number * $sumRemaining;
            break 2;
        }
    }
}
echo "Solution Day 04-1: $solution1\n";
echo "Solution Day 04-2: $solution2\n";

// -------------------------------------------------
function checkBingo(array $line): bool
{
    $countNotMarked = count(array_filter(
        $line,
        static fn(?string $value) => $value !== null
    ));
    return $countNotMarked === 0;
}


// for debugging (:
function printCards(array $cards): void
{
    foreach($cards as $index => $card) {
        printf("Card %s\n", $index + 1);
        foreach ($card as $line) {
            foreach ($line as $number) {
                printf('%02s ', $number ?? 'xx');
            }
            echo "\n";
        }
    }
    echo "\n";
}

