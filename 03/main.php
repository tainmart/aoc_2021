<?php

$inputs = array_map(static fn($line) => str_split($line), file('input', FILE_IGNORE_NEW_LINES));
$countBits = count($inputs[0]);
$solution1 = 0;
$solution2 = 0;


$countOnes = array_reduce(
    $inputs,
    static function(array $result, array $input): array {
        foreach($input as $index => $bit) {
            $result[$index] += (int)$bit === 1  ? 1 : -1;
        }
        return $result;
    },
    array_fill(0, $countBits, 0)
);
$gamma = array_map(
    static fn(int $count) => $count > 0 ? 1 : 0,
    $countOnes
);
$epsilon = array_map(
    static fn(int $count) => $count < 0 ? 1 : 0,
    $countOnes
);
$solution1 = bindec(implode('', $gamma)) * bindec(implode('', $epsilon));

$o2 = $inputs;
$co2 = $inputs;
foreach(range(0, $countBits - 1) as $bitIndex) {
    if (count($o2) > 1) {
        $column = array_column($o2, $bitIndex);
        $bit = (int) (array_sum($column) >= ceil(count($column) / 2));
        $o2 = array_filter($o2, static fn($line) => (int)$line[$bitIndex] === $bit);
    }

    if (count($co2) > 1) {
        $column = array_column($co2, $bitIndex);
        $bit = (int) (array_sum($column) >= ceil(count($column) / 2));
        $co2 = array_filter($co2, static fn($line) => (int)$line[$bitIndex] !== $bit);
    }
}
$solution2 = bindec(implode('', reset($o2))) * bindec(implode('', reset($co2)));

echo "Solution Day 03-1: $solution1\n";
echo "Solution Day 03-2: $solution2\n";
