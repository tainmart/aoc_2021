<?php
$input = file('input', FILE_IGNORE_NEW_LINES);
$bracketMap = [ ')' => '(', ']' => '[', '}' => '{', '>' => '<'];
$points = [
    '(' => 1, ')' => 3,
    '[' => 2, ']' => 57,
    '{' => 3, '}' => 1197,
    '<' => 4, '>' => 25137,
];

$solution1 = 0;
$scores = [];
foreach ($input as $line) {
    $stack1 = [];
    $stack2 = [];
    
    foreach (str_split($line) as $char) {
        if (in_array($char, $bracketMap, true)) {
            $stack1[] = $char;
            $stack2[] = $char;
            continue;
        }
        // Part 1
        if (array_pop($stack1) !== $bracketMap[$char]) {
            $solution1 += $points[$char];
        }
        // Part 2
        if (array_pop($stack2) !== $bracketMap[$char]) {
            $stack2 = [];
            break;
        }
    }
    
    if (!empty($stack2)) {
        $total = 0;
        foreach (array_reverse($stack2) as $char) {
            $total = 5 * $total + $points[$char];
        }
        $scores[] = $total;
    }
}
sort($scores);
$solution2 = $scores[floor(count($scores)/2)];

echo "Solution Day 10-1: $solution1\n";
echo "Solution Day 10-2: $solution2\n";