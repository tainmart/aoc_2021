<?php
$input = array_map(
    static function(string $line): array {
        [$patterns, $output] = explode(' | ', $line);
        
        return [
            'patterns' => array_map(static fn(string $string): string => sortChars($string), explode(' ', $patterns)),
            'output' => array_map(static fn(string $string): string => sortChars($string), explode(' ', $output)),
        ];
    },
    file('input', FILE_IGNORE_NEW_LINES)
);

$solution1 = 0;
$solution2 = 0;
foreach ($input as ['patterns' => $patterns, 'output' => $output]) {
    $signalMap = decodePatternsIntoMap($patterns);

    foreach ($output as $index => $outputDigit) {
        if (in_array(strlen($outputDigit), [2,3,4,7])) {
            $solution1++;
        }
        $solution2 += $signalMap[$outputDigit] * 10 ** (3 - $index);
    }
}


echo "Solution Day 08-1: $solution1\n";
echo "Solution Day 08-2: $solution2\n";


// ------------------------------------
function sortChars(string $string): string
{
    $chars = str_split($string);
    sort($chars);
    return implode($chars);
}

function decodePatternsIntoMap(array $patterns): array
{
    // this could be done nicer, but this was the most comfortable (:
    $one = current(array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 2));
    $four = current(array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 4));
    $seven = current(array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 3));
    $eight = current(array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 7));

    $patternLengthFive = array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 5);
    // only number 3 contains 2 signals from 1
    $three = current(array_filter($patternLengthFive, static fn(string $pattern): bool => strlen(str_replace(str_split($one), '', $pattern)) === 3));
    $patternLengthFive = array_diff($patternLengthFive, [$three]);
    // only number 5 contains 3 signals from 4
    $five = current(array_filter($patternLengthFive, static fn(string $pattern): bool => strlen(str_replace(str_split($four), '', $pattern)) === 2));
    $two = current(array_diff($patternLengthFive, [$five]));

    $patternLengthSix = array_filter($patterns, static fn(string $pattern): bool => strlen($pattern) === 6);
    // only number 6 contains 1 signal from 1
    $six = current(array_filter($patternLengthSix, static fn(string $pattern): bool => strlen(str_replace(str_split($one), '', $pattern)) === 5));
    $patternLengthSix = array_diff($patternLengthSix, [$six]);
    // only number 9 contains 5 signals from 5
    $nine = current(array_filter($patternLengthSix, static fn(string $pattern): bool => strlen(str_replace(str_split($five), '', $pattern)) === 1));
    $zero = current(array_diff($patternLengthSix, [$nine]));

    return [
        $zero => 0,
        $one => 1,
        $two => 2,
        $three => 3,
        $four => 4,
        $five => 5,
        $six => 6,
        $seven => 7,
        $eight => 8,
        $nine => 9
    ];
}
