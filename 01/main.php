<?php

$inputs = array_map('intval', file('input'));
$solution1 = 0;
$solution2 = 0;

foreach ($inputs as $index => $input) {
   if ($index < 1) {
       continue;
   } 
   $solution1 += $input > ($inputs[$index - 1] ?? null);
   
   if ($index < 3) {
       continue;
   }
   $solution2 += $input > ($inputs[$index - 3] ?? null);
}

echo "Solution Day 01-1: $solution1\n";
echo "Solution Day 01-2: $solution2\n";
