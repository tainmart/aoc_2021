<?php

['input' => $input, 'maxX' => $maxX, 'maxY' => $maxY] = array_reduce(
    file('input'),
    static function(array $result, string $line): array {
        $coords = array_map(
            static fn(string $lineCoords) => array_map('intval', explode(',', $lineCoords)),
            explode(' -> ', $line)
        );
        $result['input'][] = $coords;
        $result['maxX'] = max($result['maxX'], $coords[0][0], $coords[1][0]);
        $result['maxY'] = max($result['maxY'], $coords[0][1], $coords[1][1]);
        return $result;
    },
    ['input' => [], 'maxX' => 0, 'maxY' => 0]
);
$grid1 = $grid2 = array_fill(0, $maxX + 1, array_fill(0, $maxY + 1, 0));

foreach($input as $line) {
    $diffX = abs($line[0][0] - $line[1][0]);
    $diffY = abs($line[0][1] - $line[1][1]);

    if ($diffX && $diffY) {
        $x = $line[0][0];
        $y = $line[0][1];
        $directionX = $line[0][0] < $line[1][0] ? 1 : -1;
        $directionY = $line[0][1] < $line[1][1] ? 1 : -1;
        for($i = 0; $i <= $diffX; $i++) {
            $grid2[$y+($i*$directionY)][$x+($i*$directionX)] += 1;
        }
        continue;
    }
    $x = min($line[0][0], $line[1][0]);
    $y = min($line[0][1], $line[1][1]);
    if ($diffX) {
        for($i = 0; $i <= $diffX; $i++) {
            $grid1[$y][$x+$i] += 1;
            $grid2[$y][$x+$i] += 1;
        }
        continue;
    }
    for($i = 0; $i <= $diffY; $i++) {
        $grid1[$y+$i][$x] += 1;
        $grid2[$y+$i][$x] += 1;
    }
}

$solution1 = countGrid($grid1);
$solution2 = countGrid($grid2);
echo "Solution Day 05-1: $solution1\n";
echo "Solution Day 05-2: $solution2\n";

// --------------------------------------
function countGrid(array $grid): int
{
    array_walk_recursive(
        $grid,
        static function ($value, $index) use (&$counter) {
            $counter += (int)($value > 1);
        },
        $counter = 0
    );
    return $counter;
}
