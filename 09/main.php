<?php
const MAX_VALUE = 9;
$grid = array_map(static fn(string $line) => array_map('intval', str_split($line)), file('input', FILE_IGNORE_NEW_LINES));

$solution1 = 0;
$basins = [];
foreach ($grid as $y => $line) {
    foreach ($line as $x => $value) {
        if ($value >= ($grid[$y-1][$x] ?? MAX_VALUE)
            || $value >= ($grid[$y+1][$x] ?? MAX_VALUE)
            || $value >= ($grid[$y][$x-1] ?? MAX_VALUE)
            || $value >= ($grid[$y][$x+1] ?? MAX_VALUE)
        ) {
            continue;
        }
        $solution1 += 1 + $value;
        $basinGrid = $grid;
        $basins[] = getBasin($y, $x, $basinGrid);
    }
}
rsort($basins);
$solution2 = array_product(array_slice($basins, 0, 3));

echo "Solution Day 09-1: $solution1\n";
echo "Solution Day 09-2: $solution2\n";

//-------------------------------------
function getBasin(int $y, int $x, array &$grid): int
{
    if (($grid[$y][$x] ?? MAX_VALUE) === MAX_VALUE) {
        return 0;
    }
    $grid[$y][$x] = MAX_VALUE;

    return 1 + getBasin($y-1, $x, $grid) + getBasin($y+1, $x, $grid) + getBasin($y, $x-1, $grid) + getBasin($y, $x+1, $grid);
}
