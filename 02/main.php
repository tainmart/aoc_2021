<?php

$inputs = file('input');

$solution1 = array_reduce(
    $inputs,
    static function(array $result, string $input): array {
        [$direction, $value] = explode(' ', $input);
        switch ($direction) {
            case 'forward': $result['x'] += $value; break;
            case 'down': $result['y'] += $value; break;
            case 'up': $result['y'] -= $value; break;
        }
        return $result;
    },
    ['x' => 0, 'y' => 0]
);
$solution1 = $solution1['x'] * $solution1['y'];

$solution2 = array_reduce(
    $inputs,
    static function(array $result, string $input): array {
        [$direction, $value] = explode(' ', $input);
        switch ($direction) {
            case 'forward': $result['x'] += $value; $result['y'] += $value * $result['aim']; break;
            case 'down': $result['aim'] += $value; break;
            case 'up': $result['aim'] -= $value; break;
        }
        return $result;
    },
    ['x' => 0, 'y' => 0, 'aim' => 0]
);
$solution2 = $solution2['x'] * $solution2['y'];

echo "Solution Day 02-1: $solution1\n";
echo "Solution Day 02-2: $solution2\n";
