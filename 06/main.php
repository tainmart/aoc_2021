<?php
$input = array_map('intval', explode(',', file_get_contents('input')));
$fish =  array_count_values($input) + array_fill(0, 9,  0);
ksort($fish);

$solution1 = 0;
for($i = 0; $i < 256; $i++) {
    $newFish = array_shift($fish);
    $fish[6] += $newFish;
    $fish[] = $newFish;
    if ($i === 79) {
        $solution1 = array_sum($fish);
    }
}
$solution2 = array_sum($fish);
echo "Solution Day 06-1: $solution1\n";
echo "Solution Day 06-2: $solution2\n";
